var red = "#FF0000";
var green = "#00FF00";
var light_red = "#FCD5DC";
var light_green = "#D5FCD8";
var base_bg = "#FEFEFE";
var base_border = "#555555";

/* **************************************************************************************
 * GLOBAL
 * **************************************************************************************/

// Cookies
function setcookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getcookie(cname) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + cname + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

// Eye candies
function invalid_input(obj) {
  obj.style.backgroundColor = light_red;
  setTimeout( function() {
    obj.style.backgroundColor = base_bg;
    }
  , 2000);
}

function valid_input(obj) {
  obj.style.backgroundColor = light_green;
  obj.style.borderColor = base_border;
  setTimeout( function() {
    obj.style.backgroundColor = base_bg;
    }
  , 2000);
}

function switch_css(css) {
  var ncss = 'neutral';
  if (css == ncss) {
    ncss = 'tth';
  }
  setcookie('css', ncss, 30);
  document.location = document.location;
}

/* **************************************************************************************
 * LOGIN
 * **************************************************************************************/

function login() {
  err = false;
  username = document.getElementById('login');
  password = document.getElementById('password');
  if (username.value.length < 1) {
    err = true;
    invalid_input(username);
  }
  if (password.value.length < 1){
    err = true;
    invalid_input(password);
  }
  if (err)
    return;
  
  setcookie('login', username.value, 30);
  setcookie('password', password.value, 30);
  document.location='/';
}

function logout() {
  setcookie('token', '', 30);
  setcookie('session', '', 30);
  document.location='/';
}


/* **************************************************************************************
 * COMPONTANTS
 * **************************************************************************************/

// Update result
function update_componants() {
  obj = document.getElementById('result_container');
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    obj.innerHTML = "Erreur lors de la mise à jour de la liste (1)"
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      obj.innerHTML = "Erreur lors de la mise à jour de la liste (2)"
      } 
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      obj.innerHTML = response;
      return true;
    }
  };
  xhttp.open('POST', '/componants/update', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

// Result ordering and navigation
function update_componants_by_reference(order) {
  setcookie('c_order', order, 30);
  setcookie('c_sort', 'reference', 30);
  setcookie('c_order_refresh', '1', 30);
  update_componants();
}

function update_componants_by_designation(order) {
  setcookie('c_order', order, 30);
  setcookie('c_sort', 'designation', 30);
  setcookie('c_order_refresh', '1', 30);
  update_componants();
}

function update_componants_by_quantity(order) {
  setcookie('c_order', order, 30);
  setcookie('c_sort', 'quantity', 30);
  setcookie('c_order_refresh', '1', 30);
  update_componants();
}

function update_componants_by_place(order) {
  setcookie('c_order', order, 30);
  setcookie('c_sort', 'place', 30);
  setcookie('c_order_refresh', '1', 30);
  update_componants();
}

function c_previous_page(prevhop) {
  setcookie('c_offset', prevhop, 30);
  update_componants();
}

function c_next_page(nexthop) {
  setcookie('c_offset', nexthop, 30);
  update_componants();
}

// Search componants
function search_componants_by_reference(obj) {
  setcookie('c_reference', obj.value, 30);
  update_componants();
}

function search_componants_by_designation(obj) {
  setcookie('c_designation', obj.value, 30);
  update_componants();
}

function search_componants_by_place(obj) {
  setcookie('c_place', obj.value, 30);
  update_componants();
}

function search_componants_by_provider(obj) {
  setcookie('c_provider', obj.value, 30);
  update_componants();
}

// Delete componant
function confirm_componant_delete() {
  var msg="La suppression est définitive \net n'est pas autorisée si le \ncomposant fait partie d'un Kit.\n\nConfirmer ?";
  return confirm(msg);
}

// New componant
function new_componant() {
  var err = false;
  var obj = {};
  if (getcookie('c_count') > 0){
    var err = true;
    obj[0] = document.getElementById('reference');
  }
  if (getcookie('c_designation').length < 1){
    var err = true;
    obj[1] = document.getElementById('designation');
  }
  if (getcookie('c_place').length < 1){
    var err = true;
    obj[2] = document.getElementById('place');
  }
  if (document.getElementById('provider_id').value == 1){
    setcookie('c_provider', '2', 30);
  }
  if (err == true) {
    for (i in obj){
      invalid_input(obj[i]);
    }
    return;
  }
  create_componant();
  update_componants();
}

function create_componant() {
  var MSG='Erreur lors de la creation de la référence.';
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    alert(MSG);
    return false;
  };

  xhttp.onload = function(){
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      if (response == 'OK'){
        return true;
      }
      alert(MSG);
      return false;
    }
  };

  xhttp.open('POST', '/componants/new', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

// Update componant
function update_componant(obj, componant_id, type) {
  if (type == 'numeric') {
    if (isNaN(obj.value)) {
      alert('Valeur numérique uniquement: '+obj.value);
      return;
    }
  }
  
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    invalid_input(obj);
  };

  xhttp.onload = function(){
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      if (response == 'OK'){
        valid_input(obj);
        return;
      }
      obj.style.borderColor = red;
      invalid_input(obj);
    }
  };

  xhttp.open('POST', '/componants/update/'+componant_id, true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send('field='+obj.id+'&value='+obj.value);
}

/* **************************************************************************************
 * PROVIDERS
 * **************************************************************************************/

// Update result
function update_providers() {
  obj = document.getElementById('result_container');
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    obj.innerHTML = "Erreur lors de la mise à jour de la liste (1)"
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      obj.innerHTML = "Erreur lors de la mise à jour de la liste (2)"
      } 
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      obj.innerHTML = response;
      return true;
    }
  };
  xhttp.open('POST', '/providers/update', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

// Result ordering and navigation
function update_providers_by_name(order) {
  setcookie('p_order', order, 30);
  setcookie('p_sort', 'name', 30);
  setcookie('p_order_refresh', '1', 30);
  update_providers();
}

function update_providers_by_address(order) {
  setcookie('p_order', order, 30);
  setcookie('p_sort', 'address', 30);
  setcookie('p_order_refresh', '1', 30);
  update_providers();
}

function update_providers_by_mail(order) {
  setcookie('p_order', order, 30);
  setcookie('p_sort', 'mail', 30);
  setcookie('p_order_refresh', '1', 30);
  update_providers();
}

function update_providers_by_url(order) {
  setcookie('p_order', order, 30);
  setcookie('p_sort', 'url', 30);
  setcookie('p_order_refresh', '1', 30);
  update_providers();
}

function update_providers_by_commentl(order) {
  setcookie('p_order', order, 30);
  setcookie('p_sort', 'comment', 30);
  setcookie('p_order_refresh', '1', 30);
  update_providers();
}

function p_previous_page(prevhop) {
  setcookie('p_offset', prevhop, 30);
  update_providers();
}

function p_next_page(nexthop) {
  setcookie('p_offset', nexthop, 30);
  update_providers();
}

// Search providers
function search_providers_by_name(obj) {
  setcookie('p_name', obj.value, 30);
  update_providers();
}

function search_providers_by_address(obj) {
  setcookie('p_address', obj.value, 30);
  update_providers();
}

function search_providers_by_mail(obj) {
  setcookie('p_mail', obj.value, 30);
  update_providers();
}

function search_providers_by_url(obj) {
  setcookie('p_url', obj.value, 30);
  update_providers();
}

function search_providers_by_comment(obj) {
  setcookie('p_comment', obj.value, 30);
  update_providers();
}

// New provider
function new_provider() {
  var err = false;
  var obj = {};
  if (getcookie('p_count') > 0){
    var err = true;
    obj[0] = document.getElementById('name');
  }
  if (err == true) {
    for (i in obj){
      invalid_input(obj[i]);
    }
    return;
  }
  create_provider();
  update_providers();
}

function create_provider() {
  var MSG='Erreur lors de la creation du fournisseur.';
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    alert(MSG);
    return false;
  };

  xhttp.onload = function(){
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      if (response == 'OK'){
        return true;
      }
      alert(MSG);
      return false;
    }
  };

  xhttp.open('POST', '/providers/new', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

// Delete provider
function confirm_provider_delete() {
  var msg="La suppression est définitive \net n'est pas autorisée si le \nfournisseur est référencé \npar un composant.\n\nConfirmer ?";
  return confirm(msg);
}

// Update provider
function update_provider(obj, provider_id, type) {
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    invalid_input(obj);
  };

  xhttp.onload = function(){
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      if (response == 'OK'){
        valid_input(obj);
        return;
      }
      obj.style.borderColor = red;
      invalid_input(obj);
    }
  };

  xhttp.open('POST', '/providers/update/'+provider_id, true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send('field='+obj.id+'&value='+obj.value);
}


/* **************************************************************************************
 * KITS
 * **************************************************************************************/

// Update result
function update_kits() {
  obj = document.getElementById('result_container');
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    obj.innerHTML = "Erreur lors de la mise à jour de la liste (1)"
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      obj.innerHTML = "Erreur lors de la mise à jour de la liste (2)"
      } 
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      obj.innerHTML = response;
      return true;
    }
  };
  xhttp.open('POST', '/kits/update', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

// Result ordering and navigation
function update_kits_by_name(order) {
  setcookie('k_order', order, 30);
  setcookie('k_sort', 'name', 30);
  setcookie('k_order_refresh', '1', 30);
  update_kits();
}

function update_kits_by_name(order) {
  setcookie('k_order', order, 30);
  setcookie('k_sort', 'name', 30);
  setcookie('k_order_refresh', '1', 30);
  update_kits();
}

function update_kits_by_designation(order) {
  setcookie('k_order', order, 30);
  setcookie('k_sort', 'designation', 30);
  setcookie('k_order_refresh', '1', 30);
  update_kits();
}

function k_krevious_kage(prevhop) {
  setcookie('k_offset', prevhop, 30);
  update_kits();
}

function k_next_kage(nexthop) {
  setcookie('k_offset', nexthop, 30);
  update_kits();
}

// Search kits
function search_kits_by_name(obj) {
  setcookie('k_name', obj.value, 30);
  update_kits();
}

function search_kits_by_designation(obj) {
  setcookie('k_designation', obj.value, 30);
  update_kits();
}

// New kit
function new_kit() {
  var err = false;
  var obj = {};
  if (getcookie('k_count') > 0){
    var err = true;
    obj[0] = document.getElementById('name');
  }
  if (getcookie('k_name').length < 1){
    var err = true;
    obj[0] = document.getElementById('name');
  }
  if (getcookie('k_designation').length < 1){
    var err = true;
    obj[1] = document.getElementById('designation');
  }
  if (err == true) {
    for (i in obj){
      invalid_input(obj[i]);
    }
    return;
  }
  create_kit();
  update_kits();
}

function create_kit() {
  var MSG='Erreur lors de la creation du kit.';
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    alert(MSG);
    return false;
  };

  xhttp.onload = function(){
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      if (response == 'OK'){
        return true;
      }
      alert(MSG);
      return false;
    }
  };

  xhttp.open('POST', '/kits/new', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

// Delete kit
function confirm_kit_delete() {
  var msg="La suppression d'un kit est définitive.\n\nConfirmer ?";
  return confirm(msg);
}

// Update kit
function update_kit(obj, kit_id, type) {
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    invalid_input(obj);
  };

  xhttp.onload = function(){
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      if (response == 'OK'){
        valid_input(obj);
        return;
      }
      obj.style.borderColor = red;
      invalid_input(obj);
    }
  };

  xhttp.open('POST', '/kits/update/'+kit_id, true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send('field='+obj.id+'&value='+obj.value);
}

/* **************************************************************************************
 * KIT COMPOSITIONS
 * **************************************************************************************/

// Update kit composition
function update_kit_composition(kit_id) {
  obj = document.getElementById('kit_composition');
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    obj.innerHTML = "Erreur lors de la mise à jour de la liste (1)"
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      obj.innerHTML = "Erreur lors de la mise à jour de la liste (2)"
      } 
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      obj.innerHTML = response;
      return true;
    }
  };
  
  xhttp.open('POST', '/kits/composition/'+kit_id, true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

// Remove componant from kit
function confirm_componant_remove(designation) {
  var msg="Supprimer le composant \n'"+designation+"'\ndu kit ?";
  return confirm(msg);
}

// Add componant to kit
function add_kit_componant(kit_id, componant_id) {
  var quantity = prompt("Quantité");
  if (quantity < 1){
    alert('La quantité doit être au moins égale à 1');
    return;
  }
  if (isNaN(quantity)){
    alert('La quantité doit être un nombre.');
    return;
  }
  
  setcookie('kc_quantity', quantity, 30);
  setcookie('kc_componant_id', componant_id, 30);
  var xhttp = new XMLHttpRequest();
  
  xhttp.onerror = function(){
    alert("Erreur lors de l'ajout du composant (1).");
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      alert("Erreur lors de l'ajout du composant (2).");
      } 
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      update_kit_composition(kit_id);
      return true;
    }
  };
  
  xhttp.open('POST', '/kits/composition/add/'+kit_id, true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

// Edit componant_composition quantity
function edit_kit_composition(kit_id, componant_id){
  var quantity = prompt('Nouvelle quantité');
  if (quantity < 1){
    alert('La quantité doit être au moins égale à 1.');
    return;
  }
    
  setcookie('kc_quantity', quantity, 30);
  setcookie('kc_componant_id', componant_id, 30);

  var xhttp = new XMLHttpRequest();
  
  xhttp.onerror = function(){
    alert("Erreur lors de la mise à jour de la liste (1)");
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      alert("Erreur lors de la mise à jour de la liste (2)");
      } 
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      update_kit_composition(kit_id);
      return true;
    }
  };
  
  xhttp.open('POST', '/kits/composition/update/'+kit_id, true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}
// Update search result
function update_kit_componants(kit_id) {
  obj = document.getElementById('result_container');
  var xhttp = new XMLHttpRequest();
  xhttp.onerror = function(){
    obj.innerHTML = "Erreur lors de la mise à jour de la liste (1)"
  };

  xhttp.onload = function(){
    if (xhttp.status != 200) {
      obj.innerHTML = "Erreur lors de la mise à jour de la liste (2)"
      } 
  };

  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = xhttp.responseText;
      obj.innerHTML = response;
      return true;
    }
  };
  xhttp.open('POST', '/kits/componants/update/'+kit_id, true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send();
}

// Search componants
function search_kit_componants_by_reference(obj, kit_id) {
  setcookie('kc_reference', obj.value, 30);
  update_kit_componants(kit_id);
}

function search_kit_componants_by_designation(obj, kit_id) {
  setcookie('kc_designation', obj.value, 30);
  update_kit_componants(kit_id);
}
